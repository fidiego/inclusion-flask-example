### FLASK
from flask import Flask
from flask import render_template
from flask import request, jsonify
import json
app = Flask(__name__)


@app.route('/api/v1/hello', methods=['GET'])
def api_hello():
    """ says hello """
    return jsonify(**{'message': 'Hello Word'})


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
